package by.instr.appointment.manager.service;

import by.instr.appointment.manager.dto.adminDto.AdminCreateDto;
import by.instr.appointment.manager.dto.adminDto.AdminFullDto;
import by.instr.appointment.manager.dto.adminDto.AdminPreviewDto;
import by.instr.appointment.manager.dto.adminDto.AdminUpdateDto;
import by.instr.appointment.manager.exception.InvalidDtoException;
import by.instr.appointment.manager.exception.MissedUpdateException;

import java.util.List;

public interface AdminService {

    List<AdminPreviewDto> findAll();

    AdminFullDto findById(Long id);

    AdminFullDto create(AdminCreateDto adminCreateDto) throws InvalidDtoException;

    AdminFullDto update(AdminUpdateDto adminUpdateDto) throws MissedUpdateException;

    void deleteById(Long id);
}
