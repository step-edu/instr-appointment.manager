package by.instr.appointment.manager.service;

import by.instr.appointment.manager.dto.commentDto.CommentCreateDto;
import by.instr.appointment.manager.dto.commentDto.CommentFullDto;
import by.instr.appointment.manager.dto.commentDto.CommentPreviewDto;
import by.instr.appointment.manager.dto.commentDto.CommentUpdateDto;
import by.instr.appointment.manager.exception.InvalidDtoException;
import by.instr.appointment.manager.exception.MissedUpdateException;

import java.util.List;

public interface CommentService {

    List<CommentPreviewDto> findAll();

    CommentFullDto findById(Long id);

    CommentFullDto create(CommentCreateDto createDto) throws InvalidDtoException;

    CommentFullDto update(CommentUpdateDto updateDto) throws MissedUpdateException;

    void deleteById(Long id);
}
