package by.instr.appointment.manager.service;

import by.instr.appointment.manager.dto.appointmentDto.AppointmentCreateDto;
import by.instr.appointment.manager.dto.appointmentDto.AppointmentFullDto;
import by.instr.appointment.manager.dto.appointmentDto.AppointmentPreviewDto;
import by.instr.appointment.manager.dto.appointmentDto.AppointmentUpdateDto;
import by.instr.appointment.manager.exception.InvalidDtoException;
import by.instr.appointment.manager.exception.MissedUpdateException;

import java.util.List;

public interface AppointmentService {

    List<AppointmentPreviewDto> findAll();

    AppointmentFullDto findById(Long id);

    AppointmentFullDto create(AppointmentCreateDto createDto) throws InvalidDtoException;

    AppointmentFullDto update(AppointmentUpdateDto updateDto) throws MissedUpdateException;

    void deleteById(Long id);
}
