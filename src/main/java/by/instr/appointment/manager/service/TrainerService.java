package by.instr.appointment.manager.service;

import by.instr.appointment.manager.dto.trainerDto.TrainerCreateDto;
import by.instr.appointment.manager.dto.trainerDto.TrainerFullDto;
import by.instr.appointment.manager.dto.trainerDto.TrainerPreviewDto;
import by.instr.appointment.manager.dto.trainerDto.TrainerUpdateDto;
import by.instr.appointment.manager.exception.InvalidDtoException;
import by.instr.appointment.manager.exception.MissedUpdateException;

import java.util.List;

public interface TrainerService {

    List<TrainerPreviewDto> findAll();

    TrainerFullDto findById(Long id);

    TrainerFullDto create(TrainerCreateDto trainerCreateDto) throws InvalidDtoException;

    TrainerFullDto update(TrainerUpdateDto trainerUpdateDto) throws MissedUpdateException;

    void deleteById(Long id);
}
