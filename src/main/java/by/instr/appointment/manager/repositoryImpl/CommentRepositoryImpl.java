package by.instr.appointment.manager.repositoryImpl;

import by.instr.appointment.manager.entity.CommentEntity;
import by.instr.appointment.manager.repository.CommentRepository;
import by.instr.appointment.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentRepositoryImpl implements CommentRepository {
    @Override
    public List<CommentEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<CommentEntity> foundComments = em.createNativeQuery("SELECT * FROM comment",
                CommentEntity.class).getResultList();

        em.close();
        System.out.println("found " + foundComments.size() + " comments");
        return foundComments;
    }

    @Override
    public CommentEntity findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        CommentEntity foundComment = em.find(CommentEntity.class, id);

        em.close();
        System.out.println("Comment one trainer: " + foundComment);
        return foundComment;
    }

    @Override
    public CommentEntity create(CommentEntity commentEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.persist(commentEntity);
        em.getTransaction().commit();

        em.close();
        System.out.println("Comment was created. id: " + commentEntity.getId());
        return commentEntity;
    }

    @Override
    public CommentEntity update(CommentEntity commentEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.persist(commentEntity);
        em.getTransaction().commit();

        em.close();
        System.out.println("Comment was updated. id: " + commentEntity.getId());
        return commentEntity;

    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        CommentEntity commentDeleted = em.find(CommentEntity.class, id);
        em.remove(commentDeleted);
        em.getTransaction().commit();

        System.out.println("Comment was deleted");
        em.close();

    }
}
