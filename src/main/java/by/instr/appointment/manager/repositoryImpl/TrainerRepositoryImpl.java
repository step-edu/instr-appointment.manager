package by.instr.appointment.manager.repositoryImpl;

import by.instr.appointment.manager.entity.TrainerEntity;
import by.instr.appointment.manager.repository.TrainerRepository;
import by.instr.appointment.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.util.List;

public class TrainerRepositoryImpl implements TrainerRepository {
    @Override
    public List<TrainerEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<TrainerEntity> foundTrainers = em.createNativeQuery("SELECT * FROM trainer", TrainerEntity.class)
                .getResultList();

        em.close();
        System.out.println("found " + foundTrainers.size() + " trainers");
        return foundTrainers;
    }

    @Override
    public TrainerEntity findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        TrainerEntity foundTrainer = em.find(TrainerEntity.class, id);
        Hibernate.initialize(foundTrainer.getAppointments());

        em.close();
        System.out.println("Found one trainer: " + foundTrainer);
        return foundTrainer;
    }

    @Override
    public TrainerEntity create(TrainerEntity trainerEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.persist(trainerEntity);
        em.getTransaction().commit();

        em.close();
        System.out.println("Trainer was created. id: " + trainerEntity.getId());
        return trainerEntity;
    }

    @Override
    public TrainerEntity update(TrainerEntity trainerEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.persist(trainerEntity);
        em.getTransaction().commit();

        em.close();
        System.out.println("Trainer was updated. id: " + trainerEntity.getId());
        return trainerEntity;
    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        TrainerEntity trainerDeleted = em.find(TrainerEntity.class, id);
        em.remove(trainerDeleted);
        em.getTransaction().commit();

        System.out.println("Trainer was deleted");
        em.close();

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.createNativeQuery("DELETE FROM trainer").executeUpdate();
        em.getTransaction().commit();

        em.close();
    }
}
