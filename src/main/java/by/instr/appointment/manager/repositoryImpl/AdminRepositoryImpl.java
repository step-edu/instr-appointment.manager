package by.instr.appointment.manager.repositoryImpl;

import by.instr.appointment.manager.entity.AdminEntity;
import by.instr.appointment.manager.repository.AdminRepository;
import by.instr.appointment.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class AdminRepositoryImpl implements AdminRepository {
    @Override
    public List<AdminEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<AdminEntity> foundAdmins = em.createNativeQuery("SELECT * FROM admin", AdminEntity.class)
                .getResultList();

        em.close();
        System.out.println("found " + foundAdmins.size() + " admins");
        return foundAdmins;
    }

    @Override
    public AdminEntity findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        AdminEntity foundAdmin = em.find(AdminEntity.class, id);
        em.close();
        System.out.println("Found one admin: " + foundAdmin);
        return foundAdmin;
    }

    @Override
    public AdminEntity create(AdminEntity adminEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.persist(adminEntity);
        em.getTransaction().commit();

        em.close();
        System.out.println("Trainer was created. id: " + adminEntity.getId());
        return adminEntity;
    }

    @Override
    public AdminEntity update(AdminEntity adminEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.persist(adminEntity);
        em.getTransaction().commit();

        em.close();
        System.out.println("Trainer was updated. id: " + adminEntity.getId());
        return adminEntity;

    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        AdminEntity adminDeleted = em.find(AdminEntity.class, id);
        em.remove(adminDeleted);
        em.getTransaction().commit();

        System.out.println("Admin was deleted");
        em.close();

    }
}
