package by.instr.appointment.manager.repositoryImpl;

import by.instr.appointment.manager.entity.AppointmentEntity;
import by.instr.appointment.manager.repository.AppointmentRepository;
import by.instr.appointment.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class AppointmentRepositoryImpl implements AppointmentRepository {
    @Override
    public List<AppointmentEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<AppointmentEntity> foundAppointments = em.createNativeQuery("SELECT * FROM appointment",
                AppointmentEntity.class).getResultList();

        em.close();
        System.out.println("found " + foundAppointments.size() + " appointments");
        return foundAppointments;
    }

    @Override
    public AppointmentEntity findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        AppointmentEntity foundAppointment = em.find(AppointmentEntity.class, id);

        em.close();
        System.out.println("Found one appointment: " + foundAppointment);
        return foundAppointment;
    }

    @Override
    public AppointmentEntity create(AppointmentEntity appointmentEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.persist(appointmentEntity);
        em.getTransaction().commit();

        em.close();
        System.out.println("Appointment was created. id: " + appointmentEntity.getId());
        return appointmentEntity;
    }

    @Override
    public AppointmentEntity update(AppointmentEntity appointmentEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.persist(appointmentEntity);
        em.getTransaction().commit();

        em.close();
        System.out.println("Appointment was updated. id: " + appointmentEntity.getId());
        return appointmentEntity;

    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        AppointmentEntity appointmentDeleted = em.find(AppointmentEntity.class, id);
        em.remove(appointmentDeleted);
        em.getTransaction().commit();

        System.out.println("Appointment was deleted");
        em.close();

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();
        em.createNativeQuery("DELETE FROM appointment").executeUpdate();
        em.getTransaction().commit();

        em.close();

    }
}
