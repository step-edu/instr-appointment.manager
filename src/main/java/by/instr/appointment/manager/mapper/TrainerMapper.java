package by.instr.appointment.manager.mapper;

import by.instr.appointment.manager.dto.trainerDto.TrainerCreateDto;
import by.instr.appointment.manager.dto.trainerDto.TrainerFullDto;
import by.instr.appointment.manager.dto.trainerDto.TrainerPreviewDto;
import by.instr.appointment.manager.dto.trainerDto.TrainerUpdateDto;
import by.instr.appointment.manager.entity.TrainerEntity;

import java.util.ArrayList;
import java.util.List;

public class TrainerMapper {

    public List<TrainerPreviewDto> mapToDtoList (List<TrainerEntity> trainerEntities) {// здесь мы в
        // параметр пинимаем лист ентити траинеров из БД, и возвращаем previewDto тренеров. Из ентити, превращаем
        // в дто.

        List<TrainerPreviewDto> dtos = new ArrayList<>();

        for (TrainerEntity entity : trainerEntities) {// берем каждую ентити по отдельности

            TrainerPreviewDto dto = new TrainerPreviewDto();// создаем новый дто

            dto.setId(entity.getId());// переливаем данные из ентити
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setWorkExperience(entity.getWorkExperience());
            dto.setAchievements(entity.getAchievements());
            dto.setImageUrl(entity.getImageUrl());

            dtos.add(dto);// добавляем в список
        }

        return dtos;
    }

    public TrainerEntity mapToEntity(TrainerCreateDto trainerCreateDto) {// возвращаем ентити, из
        // из createDto

        TrainerEntity trainer = new TrainerEntity();

        trainer.setName(trainerCreateDto.getName());
        trainer.setLastName(trainerCreateDto.getLastName());
        trainer.setWorkExperience(trainerCreateDto.getWorkExperience());
        trainer.setAchievements(trainerCreateDto.getAchievements());
        trainer.setAchievements(trainerCreateDto.getAchievements());
        trainer.setImageUrl(trainerCreateDto.getImageUrl());
        trainer.setEmail(trainerCreateDto.getEmail());
        trainer.setPassword(trainerCreateDto.getPassword());


        return trainer;
    }

    public TrainerEntity mapToEntityUpdate(TrainerUpdateDto trainerUpdateDto) {

        TrainerEntity trainer = new TrainerEntity();

        trainer.setWorkExperience(trainerUpdateDto.getWorkExperience());
        trainer.setAchievements(trainerUpdateDto.getAchievements());
        trainer.setEmail(trainerUpdateDto.getEmail());
        trainer.setPassword(trainerUpdateDto.getPassword());

        return  trainer;
    }

    public TrainerFullDto mapToDto(TrainerEntity entity) {

        TrainerFullDto fullDto = new TrainerFullDto();

        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setWorkExperience(entity.getWorkExperience());
        fullDto.setAchievements(entity.getAchievements());
        fullDto.setEmail(entity.getEmail());
        fullDto.setImageUrl(entity.getImageUrl());
        fullDto.setPassword(entity.getPassword());
        fullDto.setComments(entity.getComments());
        fullDto.setAppointments(entity.getAppointments());

        return fullDto;

    }
}
