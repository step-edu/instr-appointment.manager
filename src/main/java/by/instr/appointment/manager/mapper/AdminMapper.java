package by.instr.appointment.manager.mapper;

import by.instr.appointment.manager.dto.adminDto.AdminCreateDto;
import by.instr.appointment.manager.dto.adminDto.AdminFullDto;
import by.instr.appointment.manager.dto.adminDto.AdminPreviewDto;
import by.instr.appointment.manager.dto.adminDto.AdminUpdateDto;
import by.instr.appointment.manager.entity.AdminEntity;

import java.util.ArrayList;
import java.util.List;

public class AdminMapper {

    public List<AdminPreviewDto> mapToDtoList (List<AdminEntity> entities) {

        List<AdminPreviewDto> dtos = new ArrayList<>();

        for (AdminEntity entity : entities) {

            AdminPreviewDto dto = new AdminPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setRole(entity.getRole());

            dtos.add(dto);
        }
        return dtos;
    }

    public AdminEntity mapToEntityCreate(AdminCreateDto createDto) {

        AdminEntity admin = new AdminEntity();

        admin.setName(createDto.getName());
        admin.setLastName(createDto.getLastName());
        admin.setEmail(createDto.getEmail());
        admin.setPassword(createDto.getPassword());
        admin.setRole(createDto.getRole());

        return admin;
    }

    public AdminEntity mapToEntityUpdate(AdminUpdateDto updateDto) {

        AdminEntity admin = new AdminEntity();

        admin.setId(updateDto.getId());
        admin.setEmail(updateDto.getEmail());
        admin.setPassword(updateDto.getPassword());
        admin.setRole(updateDto.getRole());

        return admin;
    }

    public AdminFullDto mapToDo(AdminEntity entity) {

        AdminFullDto fullDto = new AdminFullDto();

        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setEmail(entity.getEmail());
        fullDto.setPassword(entity.getPassword());
        fullDto.setRole(entity.getRole());

        return fullDto;
    }
}
