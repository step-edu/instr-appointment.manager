package by.instr.appointment.manager.mapper;

import by.instr.appointment.manager.dto.appointmentDto.AppointmentCreateDto;
import by.instr.appointment.manager.dto.appointmentDto.AppointmentFullDto;
import by.instr.appointment.manager.dto.appointmentDto.AppointmentPreviewDto;
import by.instr.appointment.manager.dto.appointmentDto.AppointmentUpdateDto;
import by.instr.appointment.manager.entity.AppointmentEntity;
import by.instr.appointment.manager.entity.TrainerEntity;

import java.util.ArrayList;
import java.util.List;

public class AppointmentMapper {

        public List<AppointmentPreviewDto> mapToDoList (List<AppointmentEntity> entities) {

            List<AppointmentPreviewDto> dtos = new ArrayList<>();

            for (AppointmentEntity entity: entities) {

                AppointmentPreviewDto dto = new AppointmentPreviewDto();

                dto.setId(entity.getId());
                dto.setName(entity.getName());
                dto.setLastName(entity.getLastName());
                dto.setTime(entity.getTime());
                dto.setPhoneNumber(entity.getPhoneNumber());
                dto.setTrainerId(entity.getTrainer().getId());

                dtos.add(dto);
            }
            return dtos;
        }

        public AppointmentEntity mapToEntityCreate(AppointmentCreateDto createDto,
                                                   TrainerEntity trainer) {

            AppointmentEntity appointment = new AppointmentEntity();

            appointment.setTrainer(trainer);
            appointment.setTime(createDto.getTime());
            appointment.setName(createDto.getName());
            appointment.setLastName(createDto.getLastName());
            appointment.setPhoneNumber(createDto.getPhoneNumber());
            
            return appointment;
        }

        public AppointmentEntity mapToEntityUpdate(AppointmentUpdateDto updateDto) {
            //updateDto в entity

            //создаем новый entity
            AppointmentEntity appointment = new AppointmentEntity();

            //переливаем данные с dto в entity
            appointment.setId(updateDto.getId());
            appointment.setTime(updateDto.getTime());

            return appointment;
        }

        public AppointmentFullDto mapToDto(AppointmentEntity entity) {

            AppointmentFullDto fullDto = new AppointmentFullDto();

            fullDto.setId(entity.getId());
            fullDto.setTrainerId(fullDto.getTrainerId());
            fullDto.setTime(entity.getTime());
            fullDto.setPostComment(entity.getPostComment());
            fullDto.setName(entity.getName());
            fullDto.setLastName(entity.getLastName());
            fullDto.setEmail(entity.getEmail());
            fullDto.setPhoneNumber(entity.getPhoneNumber());

            return fullDto;

        }



}
