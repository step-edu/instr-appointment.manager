package by.instr.appointment.manager.mapper;

import by.instr.appointment.manager.dto.commentDto.CommentCreateDto;
import by.instr.appointment.manager.dto.commentDto.CommentFullDto;
import by.instr.appointment.manager.dto.commentDto.CommentPreviewDto;
import by.instr.appointment.manager.dto.commentDto.CommentUpdateDto;
import by.instr.appointment.manager.entity.CommentEntity;
import by.instr.appointment.manager.entity.TrainerEntity;

import java.util.ArrayList;
import java.util.List;

public class CommentMapper {

    public List<CommentPreviewDto> mapToDtoList (List<CommentEntity> commentEntities) {

        List<CommentPreviewDto> dtos = new ArrayList<>();

        for (CommentEntity entity : commentEntities) {

            CommentPreviewDto dto = new CommentPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setMessage(entity.getMessage());
            dto.setTrainerId(entity.getTrainer().getId());

            dtos.add(dto);
        }
        return dtos;
    }

    public CommentEntity mapToEntityCreate(CommentCreateDto createDto, TrainerEntity trainer) {

        CommentEntity comment = new CommentEntity();

        comment.setTrainer(trainer);
        comment.setMessage(createDto.getMessage());
        comment.setName(createDto.getName());
        comment.setLastName(createDto.getLastName());
        comment.setGrade(createDto.getGrade());

        return  comment;
    }

    public CommentEntity mapToEntityUpdate(CommentUpdateDto updateDto) {

        CommentEntity comment = new CommentEntity();

        comment.setId(updateDto.getId());
        comment.setMessage(updateDto.getMessage());
        comment.setGrade(updateDto.getGrade());
        comment.setPublished(updateDto.getPublished());

        return comment;
    }

    public CommentFullDto mapToDo(CommentEntity entity) {

        CommentFullDto fullDto = new CommentFullDto();

        fullDto.setId(entity.getId());
        fullDto.setMessage(entity.getMessage());
        fullDto.setName(entity.getName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setGrade(entity.getGrade());
        fullDto.setPublished(entity.getPublished());
        fullDto.setTrainerId(entity.getTrainer().getId());

        return fullDto;
    }
}
