package by.instr.appointment.manager.serviceImpl;

import by.instr.appointment.manager.dto.adminDto.AdminCreateDto;
import by.instr.appointment.manager.dto.adminDto.AdminFullDto;
import by.instr.appointment.manager.dto.adminDto.AdminPreviewDto;
import by.instr.appointment.manager.dto.adminDto.AdminUpdateDto;
import by.instr.appointment.manager.entity.AdminEntity;
import by.instr.appointment.manager.exception.InvalidDtoException;
import by.instr.appointment.manager.exception.MissedUpdateException;
import by.instr.appointment.manager.mapper.AdminMapper;
import by.instr.appointment.manager.repository.AdminRepository;
import by.instr.appointment.manager.repositoryImpl.AdminRepositoryImpl;
import by.instr.appointment.manager.service.AdminService;

import java.util.List;

public class AdminServiceImpl implements AdminService {

    private AdminRepository adminRepository = new AdminRepositoryImpl();
    private AdminMapper adminMapper = new AdminMapper();


    @Override
    public List<AdminPreviewDto> findAll() {

        List<AdminEntity> found = adminRepository.findAll();

        System.out.println("AdminServiceImpl -> " + found.size() + " admins");
        return adminMapper.mapToDtoList(found);
    }

    @Override
    public AdminFullDto findById(Long id) {

        AdminEntity found = adminRepository.findById(id);
        System.out.println("AdminServiceImpl -> found admin " + found);
        return adminMapper.mapToDo(found);
    }

    @Override
    public AdminFullDto create(AdminCreateDto createDto) throws InvalidDtoException {

        validateCreateDto(createDto);

        AdminEntity toSave = adminMapper.mapToEntityCreate(createDto);
        AdminEntity created = adminRepository.create(toSave);

        System.out.println("AdminServiceImpl -> created admin: " + created);
        return adminMapper.mapToDo(created);
    }

    @Override
    public AdminFullDto update(AdminUpdateDto updateDto) throws MissedUpdateException {

        validateUpdateDto(updateDto);

        AdminEntity toUpdate = adminMapper.mapToEntityUpdate(updateDto);
        AdminEntity existingEntity = adminRepository.findById(updateDto.getId());

        toUpdate.setName(existingEntity.getName());
        toUpdate.setLastName(existingEntity.getLastName());

        AdminEntity updated = adminRepository.update(toUpdate);

        System.out.println("AdminServiceImpl -> updated admin: " + updated);
        return adminMapper.mapToDo(updated);
    }

    @Override
    public void deleteById(Long id) {

        adminRepository.deleteById(id);
        System.out.println("AdminServiceImpl -> admin deleted: " + id);
    }

    private void validateCreateDto(AdminCreateDto dto) throws InvalidDtoException {

        if (dto.getName() == null || dto.getLastName() == null ||
                dto.getEmail() == null || dto.getRole() == null) {

            throw new InvalidDtoException("One or more fields is null");
        }

        if (!dto.getEmail().contains("@")){

            throw new InvalidDtoException("Does not contain @ sign");
        }


    }

    private void validateUpdateDto(AdminUpdateDto dto) throws MissedUpdateException {
        if (dto.getId() == null) {

            throw new MissedUpdateException("Id is not specified");
        }
    }

}
