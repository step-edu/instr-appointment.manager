package by.instr.appointment.manager.serviceImpl;

import by.instr.appointment.manager.dto.commentDto.CommentCreateDto;
import by.instr.appointment.manager.dto.commentDto.CommentFullDto;
import by.instr.appointment.manager.dto.commentDto.CommentPreviewDto;
import by.instr.appointment.manager.dto.commentDto.CommentUpdateDto;
import by.instr.appointment.manager.entity.CommentEntity;
import by.instr.appointment.manager.entity.TrainerEntity;
import by.instr.appointment.manager.exception.InvalidDtoException;
import by.instr.appointment.manager.exception.MissedUpdateException;
import by.instr.appointment.manager.mapper.CommentMapper;
import by.instr.appointment.manager.repository.CommentRepository;
import by.instr.appointment.manager.repository.TrainerRepository;
import by.instr.appointment.manager.repositoryImpl.CommentRepositoryImpl;
import by.instr.appointment.manager.repositoryImpl.TrainerRepositoryImpl;
import by.instr.appointment.manager.service.CommentService;

import java.util.List;

public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository = new CommentRepositoryImpl();
    private CommentMapper commentMapper = new CommentMapper();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();


    @Override
    public List<CommentPreviewDto> findAll() {

        List<CommentEntity> foundComments = commentRepository.findAll();
        System.out.println("CommentServiceImpl -> " + foundComments.size() + " comments");

        return commentMapper.mapToDtoList(foundComments);
    }

    @Override
    public CommentFullDto findById(Long id) {

        CommentEntity found = commentRepository.findById(id);
        System.out.println("CommentServiceImpl -> found comment: " + found);
        return commentMapper.mapToDo(found);
    }

    @Override
    public CommentFullDto create(CommentCreateDto createDto) throws InvalidDtoException {

        validateCreateDto(createDto);

        TrainerEntity trainer = trainerRepository.findById(createDto.getTrainerId());
        CommentEntity toSave = commentMapper.mapToEntityCreate(createDto, trainer);
        CommentEntity created = commentRepository.create(toSave);

        System.out.println("CommentServiceImpl -> created comment: " + created);
        return commentMapper.mapToDo(created);
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateDto) throws MissedUpdateException {

        validateUpdateDto(updateDto);

        CommentEntity toUpdated = commentMapper.mapToEntityUpdate(updateDto);
        CommentEntity existingEntity = commentRepository.findById(updateDto.getId());

        toUpdated.setName(existingEntity.getName());
        toUpdated.setLastName(existingEntity.getLastName());
        toUpdated.setTrainer(existingEntity.getTrainer());

        CommentEntity updated = commentRepository.update(toUpdated);

        System.out.println("CommentServiceImpl -> updated comment: " + updated);
        return commentMapper.mapToDo(updated);
    }

    @Override
    public void deleteById(Long id) {

        commentRepository.deleteById(id);
        System.out.println("CommentServiceImpl -> deleted comment" + id);
    }

    private void validateCreateDto(CommentCreateDto createDto) throws InvalidDtoException {
        if (createDto.getMessage() == null || createDto.getName() == null ||
                createDto.getLastName() == null || createDto.getTrainerId() == null) {

            throw new InvalidDtoException("One or more fields is null");
        }
        if (createDto.getGrade() < 1 && createDto.getGrade() > 10  ) {

            throw new InvalidDtoException("Wrong estimate");

        }
    }

    private void validateUpdateDto(CommentUpdateDto updateDto) throws MissedUpdateException {
        if (updateDto.getId() == null) {

            throw new MissedUpdateException("Id is not specified");
        }
    }

}
