package by.instr.appointment.manager.serviceImpl;

import by.instr.appointment.manager.dto.appointmentDto.AppointmentCreateDto;
import by.instr.appointment.manager.dto.appointmentDto.AppointmentFullDto;
import by.instr.appointment.manager.dto.appointmentDto.AppointmentPreviewDto;
import by.instr.appointment.manager.dto.appointmentDto.AppointmentUpdateDto;
import by.instr.appointment.manager.entity.AppointmentEntity;
import by.instr.appointment.manager.entity.TrainerEntity;
import by.instr.appointment.manager.exception.InvalidDtoException;
import by.instr.appointment.manager.exception.MissedUpdateException;
import by.instr.appointment.manager.mapper.AppointmentMapper;
import by.instr.appointment.manager.repository.AppointmentRepository;
import by.instr.appointment.manager.repository.TrainerRepository;
import by.instr.appointment.manager.repositoryImpl.AppointmentRepositoryImpl;
import by.instr.appointment.manager.repositoryImpl.TrainerRepositoryImpl;
import by.instr.appointment.manager.service.AppointmentService;

import java.util.List;

public class AppointmentServiceImpl implements AppointmentService {

    private AppointmentRepository appointmentRepository = new AppointmentRepositoryImpl();
    private AppointmentMapper appointmentMapper = new AppointmentMapper();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();


    @Override
    public List<AppointmentPreviewDto> findAll() {

        List<AppointmentEntity> found = appointmentRepository.findAll();
        System.out.println("AppointmentServiceImpl -> " + found.size() + " appointments");

        return appointmentMapper.mapToDoList(found);
    }

    @Override
    public AppointmentFullDto findById(Long id) {

        AppointmentEntity found = appointmentRepository.findById(id);
            System.out.println("AppointmentServiceImpl -> found appointment: " + found);
        return appointmentMapper.mapToDto(found);
        }


    @Override
    public AppointmentFullDto create(AppointmentCreateDto createDto) throws InvalidDtoException {

        validateCreateDto(createDto);

        TrainerEntity trainer = trainerRepository.findById(createDto.getTrainerId());
        AppointmentEntity toSave = appointmentMapper.mapToEntityCreate(createDto, trainer);
        AppointmentEntity created = appointmentRepository.create(toSave);

        System.out.println("AppointmentServiceImpl -> created appointment: " + created);
        return appointmentMapper.mapToDto(created);


    }

    @Override
    public AppointmentFullDto update(AppointmentUpdateDto updateDto) throws MissedUpdateException {

        validateUpdateDto(updateDto);

        AppointmentEntity toUpdate = appointmentMapper.mapToEntityUpdate(updateDto);// здесь ентити
        //которую нужно обновить. в ней старый айдишник, новые данные
        AppointmentEntity existingEntity = appointmentRepository.findById(updateDto.getId());// оригинальная
        //ентити, существующая. для того что бы данные которые не обновляются не были null.

        toUpdate.setTrainer(existingEntity.getTrainer());
        toUpdate.setPostComment(existingEntity.getPostComment());
        toUpdate.setName(existingEntity.getName());
        toUpdate.setLastName(existingEntity.getLastName());
        toUpdate.setEmail(existingEntity.getEmail());
        toUpdate.setPhoneNumber(existingEntity.getPhoneNumber());

        AppointmentEntity updated = appointmentRepository.update(toUpdate);
        System.out.println("AppointmentServiceImpl -> updated appointment: " + updated);
        return appointmentMapper.mapToDto(updated);//переводим
        //обратно в дто


    }

    @Override
    public void deleteById(Long id) {

        appointmentRepository.deleteById(id);
        System.out.println("AppointmentServiceImpl -> appointment deleted: " + id);

    }

    private void validateCreateDto(AppointmentCreateDto dto) throws InvalidDtoException {

        if (dto.getTrainerId() == null || dto.getTime() == null || dto.getName() == null ||
                dto.getLastName() == null || dto.getPhoneNumber() == null) {

            throw new InvalidDtoException("One or more fields is null");
        }
        if (dto.getPhoneNumber().length() < 9) {
            throw new InvalidDtoException("Phone number is at least 9 digits");
        }

    }
    private void validateUpdateDto(AppointmentUpdateDto dto) throws MissedUpdateException {
        if (dto.getId() == null) {

            throw new MissedUpdateException("Id is not specified");
        }
    }

}
