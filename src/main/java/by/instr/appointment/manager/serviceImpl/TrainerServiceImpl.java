package by.instr.appointment.manager.serviceImpl;

import by.instr.appointment.manager.dto.trainerDto.TrainerCreateDto;
import by.instr.appointment.manager.dto.trainerDto.TrainerFullDto;
import by.instr.appointment.manager.dto.trainerDto.TrainerPreviewDto;
import by.instr.appointment.manager.dto.trainerDto.TrainerUpdateDto;
import by.instr.appointment.manager.entity.TrainerEntity;
import by.instr.appointment.manager.exception.InvalidDtoException;
import by.instr.appointment.manager.exception.MissedUpdateException;
import by.instr.appointment.manager.mapper.TrainerMapper;
import by.instr.appointment.manager.repository.TrainerRepository;
import by.instr.appointment.manager.repositoryImpl.TrainerRepositoryImpl;
import by.instr.appointment.manager.service.TrainerService;

import java.util.List;

public class TrainerServiceImpl implements TrainerService {

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();// связю с репозиторием
    private TrainerMapper trainerMapper = new TrainerMapper();//связь с маппером

    @Override
    public List<TrainerPreviewDto> findAll() {//здесь мы хотим через метод findAll, найти всех тренеров.
        //Нам вернется previewDto(основная информация) о всех тренерах.

        List<TrainerEntity> found = trainerRepository.findAll();//через репозиторий вызываем метод findAll
        //репозиторий взаимодействует напрямую с БД. том тоже есть метод findAll, который отправляет
        //нужный запрос в БД. в переменную found ложится список тренеров(entity)
        System.out.println("TrainerServiceImpl -> " + found.size() + " trainers");
        return trainerMapper.mapToDtoList(found);// в found находится список тренеров (entity).
        // через метод mapToDtoList, мы возвращаем, видим, только их previewDto. Переливаем данные из
        // entity, который взял репозиторий в previewDto, который увидит фронт.

    }

    @Override
    public TrainerFullDto findById(Long id) {

        TrainerEntity found = trainerRepository.findById(id);
        System.out.println("TrainerServiceImpl -> found trainer: " + found);
        return trainerMapper.mapToDto(found);
    }

    @Override
    public TrainerFullDto create(TrainerCreateDto createDto) throws InvalidDtoException {

        validateCreateDto(createDto);

        TrainerEntity toSave = trainerMapper.mapToEntity(createDto);

        TrainerEntity created = trainerRepository.create(toSave);
        System.out.println("TrainerServiceImpl -> created trainer: " + created);
        return trainerMapper.mapToDto(created);
    }

    @Override
    public TrainerFullDto update(TrainerUpdateDto trainerUpdateDto) throws MissedUpdateException {

        validateUpdateDto(trainerUpdateDto);

        TrainerEntity toUpdate = trainerMapper.mapToEntityUpdate(trainerUpdateDto);
        TrainerEntity existingEntity = trainerRepository.findById(trainerUpdateDto.getId());

        toUpdate.setName(existingEntity.getName());
        toUpdate.setLastName(existingEntity.getLastName());
        toUpdate.setImageUrl(existingEntity.getImageUrl());
        toUpdate.setComments(existingEntity.getComments());
        toUpdate.setAppointments(existingEntity.getAppointments());

        TrainerEntity updated = trainerRepository.update(toUpdate);
        System.out.println("TrainerServiceImpl -> updated trainer: " + updated);
        return trainerMapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {

        trainerRepository.deleteById(id);
        System.out.println("TrainerServiceImpl -> trainer with id " + id +
                "was deletes");
    }

    private void validateCreateDto(TrainerCreateDto createDto) throws InvalidDtoException {

        if (createDto.getName() == null  || createDto.getLastName() == null ||
            createDto.getWorkExperience() == 0 || createDto.getImageUrl() == null) {
            throw new InvalidDtoException("mistake!!!!!");
        }

        if (createDto.getAchievements().length() < 2) {
            throw new InvalidDtoException("few achievements!!");
        }
    }

    private void validateUpdateDto(TrainerUpdateDto updateDto) throws MissedUpdateException {

        if (updateDto.getId() == null) {

            throw new MissedUpdateException("no id!!");
        }
    }
}
