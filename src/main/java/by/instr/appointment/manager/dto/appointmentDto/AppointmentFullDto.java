package by.instr.appointment.manager.dto.appointmentDto;

import lombok.Data;

@Data
public class AppointmentFullDto {

    private Long id;

    private Long trainerId;

    private String time;

    private String postComment;

    private String name;

    private String lastName;

    private String email;

    private String phoneNumber;

}
