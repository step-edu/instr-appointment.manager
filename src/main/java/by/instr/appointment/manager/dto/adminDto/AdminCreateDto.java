package by.instr.appointment.manager.dto.adminDto;

import by.instr.appointment.manager.entity.enums.Role;
import lombok.Data;

@Data
public class AdminCreateDto {

    private String name;

    private String lastName;

    private String email;

    private String password;

    private Role role;
}
