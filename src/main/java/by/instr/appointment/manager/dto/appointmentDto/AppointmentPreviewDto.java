package by.instr.appointment.manager.dto.appointmentDto;

import lombok.Data;

@Data
public class AppointmentPreviewDto {

    private Long id;

    private String time;

    private String name;

    private String lastName;

    private String phoneNumber;

    private Long trainerId;


}
