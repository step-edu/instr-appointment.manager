package by.instr.appointment.manager.dto.adminDto;

import by.instr.appointment.manager.entity.enums.Role;
import lombok.Data;

@Data
public class AdminPreviewDto {

    private Long id;

    private String name;

    private String lastName;

    private Role role;
}
