package by.instr.appointment.manager.dto.commentDto;

import lombok.Data;

@Data
public class CommentCreateDto {

    private Long trainerId;

    private String message;

    private String name;

    private String lastName;

    private int grade;
}
