package by.instr.appointment.manager.dto.trainerDto;

import lombok.Data;

@Data
public class TrainerUpdateDto {

    private Long id;

    private int workExperience;

    private String achievements;

    private String email;

    private String password;
}
