package by.instr.appointment.manager.dto.commentDto;

import lombok.Data;

@Data
public class CommentFullDto {

    private Long id;

    private String message;

    private String name;

    private String lastName;

    private int grade;

    private String published;

    private Long trainerId;

}
