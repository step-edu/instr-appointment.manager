package by.instr.appointment.manager.dto.appointmentDto;

import lombok.Data;

@Data
public class AppointmentCreateDto {

    private Long trainerId;

    private String time;

    private String name;

    private String lastName;

    private String phoneNumber;

}
