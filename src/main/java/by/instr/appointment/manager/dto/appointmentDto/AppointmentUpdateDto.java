package by.instr.appointment.manager.dto.appointmentDto;

import lombok.Data;

@Data
public class AppointmentUpdateDto {

    private Long id;

    private String time;

}
