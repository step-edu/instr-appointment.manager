package by.instr.appointment.manager.dto.commentDto;

import lombok.Data;

@Data
public class CommentPreviewDto {

    private Long id;

    private String name;

    private String lastName;

    private String message;

    private Long trainerId;


}
