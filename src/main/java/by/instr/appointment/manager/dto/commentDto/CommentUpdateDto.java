package by.instr.appointment.manager.dto.commentDto;

import lombok.Data;

@Data
public class CommentUpdateDto {

    private Long id;

    private String message;

    private int grade;

    private String published;
}
