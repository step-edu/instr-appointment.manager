package by.instr.appointment.manager.dto.trainerDto;

import by.instr.appointment.manager.entity.AppointmentEntity;
import by.instr.appointment.manager.entity.CommentEntity;
import lombok.Data;

import java.util.List;

@Data
public class TrainerFullDto {

    private Long id;

    private String name;

    private String lastName;

    private int workExperience;

    private String achievements;

    private String email;

    private String imageUrl;

    private String password;

    private List<CommentEntity> comments;

    private List<AppointmentEntity> appointments;
}
