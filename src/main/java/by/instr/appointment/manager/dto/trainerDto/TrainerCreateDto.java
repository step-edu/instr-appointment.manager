package by.instr.appointment.manager.dto.trainerDto;

import lombok.Data;

@Data
public class TrainerCreateDto {

    private String name;

    private String lastName;

    private int workExperience;

    private String achievements;

    private String imageUrl;

    private String email;

    private String password;
}
