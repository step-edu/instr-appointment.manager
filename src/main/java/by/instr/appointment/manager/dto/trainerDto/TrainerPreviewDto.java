package by.instr.appointment.manager.dto.trainerDto;

import lombok.Data;

@Data
public class TrainerPreviewDto {

    private Long id;

    private String name;

    private String lastName;

    private int workExperience;

    private String achievements;

    private String imageUrl;
}
