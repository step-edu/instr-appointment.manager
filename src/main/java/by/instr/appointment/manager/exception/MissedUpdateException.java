package by.instr.appointment.manager.exception;

public class MissedUpdateException extends InvalidDtoException{

    public MissedUpdateException(String message) {

        super(message);
    }
}
