package by.instr.appointment.manager.exception;

public class InvalidDtoException extends Exception {

    public InvalidDtoException(String message) {
        super("Mistake, " + message);
    }
}
