package by.instr.appointment.manager.repository;

import by.instr.appointment.manager.entity.AdminEntity;

import java.util.List;

public interface AdminRepository {

    List<AdminEntity> findAll();

    AdminEntity findById(Long id);

    AdminEntity create(AdminEntity adminEntity);

    AdminEntity update(AdminEntity adminEntity);

    void deleteById(Long id);
}
