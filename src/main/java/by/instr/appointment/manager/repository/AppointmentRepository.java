package by.instr.appointment.manager.repository;

import by.instr.appointment.manager.entity.AppointmentEntity;

import java.util.List;

public interface AppointmentRepository {

    List<AppointmentEntity> findAll();

    AppointmentEntity findById(Long id);

    AppointmentEntity create(AppointmentEntity appointmentEntity);

    AppointmentEntity update(AppointmentEntity appointmentEntity);

    void deleteById(Long id);

     void deleteAll();


}
