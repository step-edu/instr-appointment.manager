package by.instr.appointment.manager.repository;

import by.instr.appointment.manager.entity.CommentEntity;

import java.util.List;

public interface CommentRepository {

    List<CommentEntity> findAll();

    CommentEntity findById(Long id);

    CommentEntity create(CommentEntity commentEntity);

    CommentEntity update(CommentEntity commentEntity);

    void deleteById(Long id);
}
