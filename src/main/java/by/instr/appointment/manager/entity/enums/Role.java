package by.instr.appointment.manager.entity.enums;

public enum Role {

    ADMIN,
    SUPERUSER
}
