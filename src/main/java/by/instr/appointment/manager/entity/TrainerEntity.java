package by.instr.appointment.manager.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "trainer")
public class TrainerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "work_experience", nullable = false)
    private int workExperience;

    @Column(name = "achievements", nullable = false)
    private String achievements;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "image_url", nullable = false)
    private String imageUrl;

    @Column(name = "password")
    private String password;

    @EqualsAndHashCode.Exclude// что бы не попасть в безконечный цикл
    @ToString.Exclude// что бы не попасть в безконечный цикл
    @OneToMany(mappedBy = "trainer")
    private List<CommentEntity> comments;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "trainers", fetch = FetchType.EAGER)
    private List<AppointmentEntity> appointments;

}
