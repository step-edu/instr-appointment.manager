package by.instr.appointment.manager;

import by.instr.appointment.manager.entity.AppointmentEntity;
import by.instr.appointment.manager.entity.TrainerEntity;
import by.instr.appointment.manager.repositoryImpl.AdminRepositoryImpl;
import by.instr.appointment.manager.repositoryImpl.AppointmentRepositoryImpl;
import by.instr.appointment.manager.repositoryImpl.TrainerRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.instr.appointment.manager.EntityGenerate.*;

@SpringBootTest
class TrainerRepositoryTest {

	private TrainerRepositoryImpl trainerRepository = new TrainerRepositoryImpl();
	private AppointmentRepositoryImpl appointmentRepository = new AppointmentRepositoryImpl();

	@BeforeEach
	void setUp() {
		appointmentRepository.deleteAll();
		trainerRepository.deleteAll();
	}

	@Test
	void testCreate() {
		//given
		TrainerEntity trainer = generateTrainer();

		//when
		TrainerEntity saved = trainerRepository.create(trainer);

		//then
		Assertions.assertNotNull(saved.getId());

	}

	@Test
	void testFindAll() {
		//given
		List<TrainerEntity> trainers = generateListTrainers();


		for (TrainerEntity trainer : trainers) {
			trainerRepository.create(trainer);
		}

		//when
		List<TrainerEntity> foundTrainers = trainerRepository.findAll();

		//then
		Assertions.assertEquals(2,foundTrainers.size());

	}

	@Test
	void testFindById() {

		//given
		TrainerEntity trainer = generateTrainer();

		TrainerEntity saved = trainerRepository.create(trainer);

		//when
		TrainerEntity foundTrainer = trainerRepository.findById(saved.getId());

		//then
		Assertions.assertEquals(saved, foundTrainer);
		Assertions.assertNotNull(foundTrainer.getId());

	}

	@Test
	void testUpdate() {

		//given
		TrainerEntity trainer1 = generateTrainer();
		TrainerEntity saved = trainerRepository.create(trainer1);

		TrainerEntity trainer2 = generateTrainerTwo();

		//when

		trainerRepository.update(trainer2);

		//then
		Assertions.assertNotEquals(saved, trainer2);

	}

	@Test
	void testDeleteById() {

		//given
		TrainerEntity trainer = generateTrainer();
		TrainerEntity saved = trainerRepository.create(trainer);

		//when

		trainerRepository.deleteById(saved.getId());
		TrainerEntity found = trainerRepository.findById(saved.getId());

		//then
		Assertions.assertNull(found);
	}
	@Test
	void testFindByIdTrainerWithAppointment() {

		//given
		TrainerEntity trainer = generateTrainer();
		TrainerEntity savedTrainer = trainerRepository.create(trainer);

//		AppointmentEntity appointment = generateAppointment();
//		AppointmentEntity savedAppointment = appointmentRepository.create(appointment);
		AppointmentEntity a1 = AppointmentEntity.builder()
				.trainer(savedTrainer)
				.time("18.00")
				.name("Jey")
				.lastName("Lo")
				.phoneNumber("2343-5677")
				.build();

		AppointmentEntity a2 = AppointmentEntity.builder()
				.trainer(savedTrainer)
				.time("19.00")
				.name("Olga")
				.lastName("Sol")
				.phoneNumber("324-434-55")
				.build();

		appointmentRepository.create(a1);
		appointmentRepository.create(a2);

		//when
		TrainerEntity foundTrainer = trainerRepository.findById(savedTrainer.getId());

		//then
		Assertions.assertNotNull(foundTrainer.getId());
		Assertions.assertNotNull(foundTrainer.getAppointments());
		Assertions.assertEquals(2,foundTrainer.getAppointments().size());
	}

}
