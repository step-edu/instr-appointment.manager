package by.instr.appointment.manager;

import by.instr.appointment.manager.entity.AppointmentEntity;
import by.instr.appointment.manager.entity.TrainerEntity;
import lombok.Builder;

import java.util.List;

@Builder
public class EntityGenerate {

    public static TrainerEntity generateTrainer() {

        return TrainerEntity.builder()
                .name("Alex")
                .lastName("Alex")
                .workExperience(5)
                .achievements("Big")
                .imageUrl("Smile")
                .build();
    }

    public static TrainerEntity generateTrainerTwo() {

        return TrainerEntity.builder()
                .name("Bobi")
                .lastName("Bobi")
                .workExperience(8)
                .achievements("BigBobi")
                .imageUrl("SmileBobi")
                .build();
    }

    public static List<TrainerEntity> generateListTrainers() {

        return List.of(
                TrainerEntity.builder()
                        .name("Li")
                        .lastName("Umi")
                        .workExperience(6)
                        .achievements("Crawl on the chest")
                        .imageUrl("Pandora")
                        .build(),

                TrainerEntity.builder()
                        .name("Kon")
                        .lastName("U")
                        .workExperience(2)
                        .achievements("Freestyle")
                        .imageUrl("Chimera")
                        .build());
    }

//    public static AppointmentEntity generateAppointment() {
//
//        return AppointmentEntity.builder()
//                .time("18.00")
//                .name("Jey")
//                .lastName("Lo")
//                .phoneNumber("2343-5677")
//                .build();
//    }
}
